from pydantic import BaseModel, ConfigDict, Field


class AbstractModel(BaseModel):
    model_config = ConfigDict(
        extra="ignore",
        frozen=True,
        allow_inf_nan=False,
        strict=False,
        from_attributes=True,
        populate_by_name=True,
    )


class AzureSecurityConfig(AbstractModel):
    token_endpoint: str
    jwks_uri: str
    issuer: str
    authorization_endpoint: str


class OauthSecurityDetails(AbstractModel):
    tenant_id: str
    client_id: str
    client_secret: str
    scopes: list[str]
    graph_scopes: list[str] = ["User.Read"]


class AuthorizedUser(AbstractModel):
    name: str
    email: str = Field(alias="preferred_username")
    token: str
    security: OauthSecurityDetails
