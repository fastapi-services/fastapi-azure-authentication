from functools import lru_cache
from typing import Annotated

from azure.identity.aio import OnBehalfOfCredential
from msgraph import GraphServiceClient
from fastapi import Depends, HTTPException
from starlette.status import HTTP_403_FORBIDDEN

from fastapi_azure_auth.logger import logger
from fastapi_azure_auth.models import AuthorizedUser


class RoleBaseAccessControl:

    def __init__(self, user: Annotated[AuthorizedUser, Depends()]):
        credentials = OnBehalfOfCredential(tenant_id=user.security.tenant_id, client_id=user.security.client_id,
                                           client_secret=user.security.client_secret, user_assertion=user.token)
        self.client = GraphServiceClient(credentials=credentials, scopes=user.security.graph_scopes)
        self.user = user

    @lru_cache
    async def groups(self) -> list[str]:
        logger.info(f'fetching groups for user: {self.user.name}')
        groups_ = await self.client.me.get_member_groups.post()
        if groups_ is None:
            raise HTTPException(
                status_code=HTTP_403_FORBIDDEN,
                detail="Unable to get users roles",
                headers={"WWW-Authenticate": "Bearer"},
            )
        logger.info(f'fetched {len(groups_.value)} for user: {self.user.name}')
        return groups_.value
