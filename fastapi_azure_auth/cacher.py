from typing import Any, Optional

import httpx
from httpx import Response
from cryptography.hazmat.primitives.asymmetric.types import PublicKeyTypes
from jose import jwk

from fastapi_azure_auth.logger import logger
from fastapi_azure_auth.models import AzureSecurityConfig


class JwtCacher:

    def __init__(self, tenant_id: str) -> None:
        self._cache: dict[str, PublicKeyTypes] = {}
        self.config_url = f"https://login.microsoftonline.com/{tenant_id}/v2.0/.well-known/openid-configuration"
        self.azure_config: Optional[AzureSecurityConfig] = self.get_azure_config()

    async def run(self) -> None:
        logger.info("Refreshing Signing Keys")
        logger.info(f"Current Cache Size: {len(self._cache)}")
        logger.info(f"Config Url: {self.config_url}")
        keys = await self.get_jwk_keys()
        new_cache = {
            k["kid"]: jwk.construct(k, "RS256")
            for k in keys
            if k["use"] == "sig" and "kid" in k
        }
        logger.info(f"Fetched keys of size {len(new_cache)}")
        self._cache.update(new_cache)

    def get_azure_config(self) -> AzureSecurityConfig:
        with httpx.Client() as client:
            resp: Response = client.get(self.config_url)
            resp.raise_for_status()
            return AzureSecurityConfig.model_validate(resp.json())

    async def get_jwk_keys(self) -> list[dict[Any, Any]]:
        logger.info(f"Trying to fetch jwk keys from azure")
        logger.info(f"Fetching jwks from {self.azure_config.jwks_uri}")

        async with httpx.AsyncClient() as client:
            resp = await client.get(self.azure_config.jwks_uri)
            resp.raise_for_status()
            data = resp.json()
            return data["keys"]

    def get(self, key: Optional[str]) -> Optional[PublicKeyTypes]:
        if key:
            return self._cache.get(key)
