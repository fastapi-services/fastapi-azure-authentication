import logging
from logging.config import dictConfig

dictConfig(
    {
        "version": 1,
        "formatters": {
            "f": {"format": "%(asctime)s %(name)-12s %(processName)s %(threadName)s %(levelname)-8s %(message)s"}
        },
        "handlers": {
            "h": {
                "class": "logging.StreamHandler",
                "formatter": "f",
                "level": logging.INFO,
            }
        },
        "root": {"handlers": ["h"], "level": logging.INFO},
    }
)

logger = logging.getLogger("fastapi-azure-auth")