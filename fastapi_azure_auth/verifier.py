import asyncio
import time
from threading import Thread
from fastapi import HTTPException, Request
from fastapi.security import OAuth2AuthorizationCodeBearer, SecurityScopes
from fastapi.security.base import SecurityBase
from jose import jwt
from jose.exceptions import ExpiredSignatureError, JWTClaimsError, JWTError
from starlette.status import HTTP_401_UNAUTHORIZED

from fastapi_azure_auth.cacher import JwtCacher
from fastapi_azure_auth.logger import logger
from fastapi_azure_auth.models import AuthorizedUser, OauthSecurityDetails


class JwtVerifier(SecurityBase):
    def __init__(self, oauth: OauthSecurityDetails, sleep: int = 10) -> None:
        super().__init__()
        self.security = JwtCacher(tenant_id=oauth.tenant_id)
        self.oauth = OAuth2AuthorizationCodeBearer(
            authorizationUrl=self.security.azure_config.authorization_endpoint,
            tokenUrl=self.security.azure_config.token_endpoint,
            scheme_name="AzureAuthorizationCodeBearerBase",
            description="`Leave client_secret blank`",
            scopes={scope: scope for scope in oauth.scopes},
            auto_error=True,
        )
        self.details = oauth
        self.scheme_name = "AzureAuthorizationCodeBearerBase"
        self.model = self.oauth.model
        self.run(sleep)

    def run(self, sleep: int):
        def _run():
            while True:
                asyncio.run(self.security.run())
                logger.info(f'Sleeping For {sleep} seconds')
                time.sleep(sleep)

        thread = Thread(name="fastapi_azure_auth.jwt.verifier", daemon=True, target=_run)
        thread.start()

    async def __call__(self, request: Request, _: SecurityScopes) -> AuthorizedUser:
        try:
            access_token = await self.oauth(request=request)
            header: dict[str, str] = jwt.get_unverified_header(token=access_token) or {}
            user_raw = jwt.decode(
                access_token,
                key=self.security.get(header.get("kid")),
                algorithms=["RS256"],
                audience=self.details.client_id,
            )
            user = AuthorizedUser.model_validate(
                {**user_raw, "token": access_token, "security": self.details})
            logger.info(f'Successfully verified user: {user.name}')
            return user
        except (JWTError, ExpiredSignatureError, JWTClaimsError) as e:
            logger.exception(e)
            raise HTTPException(
                status_code=HTTP_401_UNAUTHORIZED,
                detail=e.args[0],
                headers={"WWW-Authenticate": "Bearer"},
            )
