# fastapi-azure-authentication

Fast API Middleware to add Azure Authentication and Role Base Access Control


## Installing Instructions

```sh
pip install fastapi-azure-authentication
```

