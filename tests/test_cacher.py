from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch, MagicMock, AsyncMock

from fastapi_azure_auth.cacher import JwtCacher
from fastapi_azure_auth.models import AzureSecurityConfig


class TestJwtCacher(IsolatedAsyncioTestCase):

    @patch("fastapi_azure_auth.cacher.httpx.AsyncClient.get", return_value=AsyncMock())
    @patch("fastapi_azure_auth.cacher.httpx.Client")
    async def test_get_jwk_keys(self, client: MagicMock, async_jwk: AsyncMock):
        resp = AzureSecurityConfig(
            token_endpoint="token",
            jwks_uri="uri",
            issuer="issuer",
            authorization_endpoint="endpoint"
        )
        client().__enter__().get().json.return_value = resp.model_dump()
        cacher = JwtCacher(tenant_id="100-000")
        mock = MagicMock()
        mock.json().__getitem__.return_value = [{'1': '1'}]
        async_jwk.return_value = mock
        keys = await cacher.get_jwk_keys()
        self.assertListEqual(keys, [{'1': '1'}])

    @patch("fastapi_azure_auth.cacher.httpx.Client")
    def test_get_azure_config(self, client: MagicMock):
        resp = AzureSecurityConfig(
            token_endpoint="token",
            jwks_uri="uri",
            issuer="issuer",
            authorization_endpoint="endpoint"
        )
        client().__enter__().get().json.return_value = resp.model_dump()
        cacher = JwtCacher(tenant_id="100-000")
        self.assertEqual(resp, cacher.azure_config)

    @patch("fastapi_azure_auth.cacher.httpx.Client")
    def test_get_key(self, client: MagicMock):
        resp = AzureSecurityConfig(
            token_endpoint="token",
            jwks_uri="uri",
            issuer="issuer",
            authorization_endpoint="endpoint"
        )
        client().__enter__().get().json.return_value = resp.model_dump()
        cacher = JwtCacher(tenant_id="100-000")
        key = cacher.get("key")
        self.assertIsNone(key)