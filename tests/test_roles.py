from unittest import IsolatedAsyncioTestCase
from unittest.mock import patch, AsyncMock, MagicMock

from fastapi import HTTPException
from msgraph.generated.applications.item.get_member_groups.get_member_groups_post_response import \
    GetMemberGroupsPostResponse

from fastapi_azure_auth.roles import RoleBaseAccessControl


class TestRoleBaseAccessControl(IsolatedAsyncioTestCase):

    @patch("fastapi_azure_auth.roles.GraphServiceClient", return_value=MagicMock())
    async def test_groups(self, client: AsyncMock):
        user = MagicMock()
        user.name = "Anon, User"
        response = AsyncMock(return_value=GetMemberGroupsPostResponse(value=["GrpA"]))
        client().me.get_member_groups.post = response
        role = RoleBaseAccessControl(user)
        groups = await role.groups()
        self.assertListEqual(groups, ["GrpA"])
        response.assert_awaited_once()

    @patch("fastapi_azure_auth.roles.GraphServiceClient", return_value=MagicMock())
    async def test_groups_error(self, client: AsyncMock):
        user = MagicMock()
        user.name = "Anon, User"
        response = AsyncMock(return_value=None)
        client().me.get_member_groups.post = response
        role = RoleBaseAccessControl(user)
        with self.assertRaises(HTTPException):
            await role.groups()
        response.assert_awaited_once()
